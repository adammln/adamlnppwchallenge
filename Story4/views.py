from django.shortcuts import render
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect


def load_base_html(request) :
    return render_to_response('base.html')

def index_view(request):
    project = Project.objects.all()
    response['project'] = project

    html = 'index.html'

    return render_to_response(request, html, response)





# Create your views here.
