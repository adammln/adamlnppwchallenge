from django.test import TestCase
from selenium import webdriver

import time
import unittest

class Story5Test(unittest.TestCase):

	def setUp(self):
		self.browser = webdriver.Chrome()

	def tearDown(self):
		self.browser.quit()

	def test_can_make_schedule(self):
		browser = webdriver.Chrome()

		browser.get('http://localhost:8000/schedule/create')
		elm = browser.find_element_by_id('f_date')
		elm.send_keys('22:11:22')
		elm = browser.find_element_by_id('f_time')
		elm.send_keys('11/09/2018')
		elm = browser.find_element_by_id('str_name')
		elm.send_keys('testing')
		elm = browser.find_element_by_id('str_loc')
		elm.send_keys('server')
		elm = browser.find_element_by_id('str_ctg')
		elm.submit()
		time.sleep(2)
		browser.quit()

	def test_layout(self):
		self.browser.get('http://localhost:8000/')
		self.assertIn(self.browser.title, "Hey, It\'s Adam")
		firstContent = self.browser.find_element_by_tag_name('p').text
		self.assertIn(firstContent,'My name is Adam Maulana,\nI’m from Indonesia and living in Jakarta.\nI study in University of Indonesia, majoring in\nInformation System.')

	def test_css(self):
		self.browser.get('http://localhost:8000/')
		el = self.browser.find_element_by_name('navbarButton')
		el.click()
		content = self.browser.find_element_by_tag_name('a').value_of_css_property('font-size')
		self.assertIn(content, '28px')
		el = self.browser.find_element_by_id('portfolio').value_of_css_property('position')
		self.assertIn(el, 'relative')


# Create your tests here.



