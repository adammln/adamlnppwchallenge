from django.contrib import admin
from django.urls import path,include
from .views import *

urlpatterns = [
	path('', jadwal_details_view, name="sched_overview"),
	path('create/', jadwal_create, name="create_event"),
	path('save/', jadwal_save, name="save_event"),
	path('delete-all/', delete_all_schedule, name="delete_all_event")
]