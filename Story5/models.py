from django.db import models

class JadwalPribadi(models.Model):
	date = models.DateField()
	time = models.TimeField()
	event_name = models.CharField(max_length=32, blank=False)
	location = models.CharField(max_length=48)
	category = models.CharField(max_length=16)



# Create your models here.
