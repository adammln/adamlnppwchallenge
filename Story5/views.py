from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect

from .forms import JadwalForm

from .models import JadwalPribadi
# Create your views here.

response = {}
def jadwal_create(request):
	response['title'] = 'Add Event'
	response['request_data_jadwal'] = JadwalForm()
	return render(request, 'create_schedule.html', response)

def jadwal_save(request):
	form = JadwalForm(request.POST or None)
	if (request.method == 'POST' and form.is_valid()):
		response['date'] = request.POST['date']	
		response['time'] = request.POST['time']
		response['event_name'] = request.POST['event_name']
		response['location'] = request.POST['location']
		response['category'] = request.POST['category']

		Jadwal_submit = JadwalPribadi(
			date=response['date'],
			time=response['time'],
			event_name=response['event_name'],
			location=response['location'], 
			category=response['category'],
			)

		Jadwal_submit.save()
		return HttpResponseRedirect('/schedule/')
	else : 	
		form = JadwalForm()
	return render(request, 'view_schedule.html')

def jadwal_details_view(request):
	schedules = JadwalPribadi.objects.all()
	response['schedules'] = schedules
	return render(request, "view_schedule.html", response)

def delete_all_schedule(request):
	JadwalPribadi.objects.all().delete()
	return HttpResponseRedirect('/schedule/')

