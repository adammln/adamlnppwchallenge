from django import forms
#from django.conf import settings

from .models import JadwalPribadi

class JadwalForm(forms.Form):
	date = forms.DateField(label='',
		required=True, 
		#input_formats=settings.DATE_INPUT_FORMATS,
		widget=forms.DateInput(attrs={'class' : 'input-group-text','id' : 'f_date', 'type':'date'}))#,'placeholder':'d/m/Y'})),
	time = forms.TimeField( label='',
		required=True,
		widget=forms.TimeInput(attrs={'class' : 'input-group-text','id' : 'f_time', 'type' : 'time'}))
	event_name = forms.CharField(label='',
		required=True,
		max_length=32,
		widget=forms.TextInput(attrs={'class' : 'input-group-text','id' : 'str_name', 'type':'text','placeholder':'Event\'s Name'}))
	location = forms.CharField(label='',
		max_length=48,
		required=False,
		widget=forms.TextInput(attrs={'class' : 'input-group-text','id' : 'str_loc', 'type':'text','placeholder':'Location'}))
	category = forms.CharField(label='',
		max_length=16,
		required=False,
		widget=forms.TextInput(attrs={'class' : 'input-group-text','id' : 'str_ctg', 'type':'text','placeholder':'Category'}))


	#class Meta:
	#	model = JadwalPribadi
	#	fields = [
	#		'event_name',
	#		'date',
	#		'time',
	#		'location',
	#		'category',
	#	]

			

